# Socratica

1 ::: [RSVPish](https://dud-poll.inf.tu-dresden.de/socratica2/)
2 ::: [Info 1](https://docs.google.com/document/d/1EzeNMlv-rt5qwNSN4ocTs5WnI2wp0AEtBVDjFAcHOAQ/edit?usp=sharing)

This will help me manage how much food to get + other logistics

## When/Where
Mient. Address upon request: socratica@tuta.io 
This Saturday arrive between 12:00 PM - 12:15 PM, I will be there earlier; please arrive by 12:10 PM at the latest, "hard start" at 12:15 PM. It's more fun when nobody is waiting to start working. 

### Disclaimer
As we ooze into the weekly rhythm of Socratica, I want to let you know that the day (Saturday vs Sunday) is subject to change, another reason to RSVP early since you can fill out your time preference. If enough people don't have an idea what this event is we might change it up a little and do a sonic walk in Den Haag market before meeting for tea and sharing experiences. Also, always open for your ideas: socratica@tuta.io and you can also just type it [here](https://docs.google.com/document/d/1EzeNMlv-rt5qwNSN4ocTs5WnI2wp0AEtBVDjFAcHOAQ/edit?usp=sharing)

## Here's what's on the agenda
- 12:00 PM - 12:15 PM: Arrive
- 12:15 PM - 12:30 PM: Beginning ritual
- 12:30 PM - 1:45 PM: Deep Work
- 1:45 PM - 2:00 PM: Opt-in Demos/ Closing

After the demos, we will hang out together, share ideas, make plans for the rest of Socratica 2024, meet old friends and make new ones.

## What's this?
A couple of individuals keeping each other accountable, working in sessions of 50 minutes of work and 10 minutes of break time. Don't have anything to present? Don't worry! We'll find something for you to work on during the session

### Can be anything
(e.g., growing fungi, building an instrument, programming an app, knitting, painting custom shoes) as long as it's something you want to do and not your main "project". More fun this way. No school work for students, no job work for the employed. You are very welcome to come and observe too! 

### Demos
At the end, we'll have 2-minute demos for people to talk about what they worked on - it's opt-in! For the first session we 7 people in total, despite more people showing instrument.

### Regular Demo Format
Demo can be on any kind of project you've been working on recently. Tell a story! You might want to include:

- ✨ Your inspiration
- ✨ Your process
- 📺 Show off what you made!
- 🔮 What's next

Impromptu presentations are expected to be 1 minute, so if you just started on the day of the event, you're good to go

## Who's it for?
Participants: 5-20 people willing to work on anything other than their main "project" (that means no school work if you're a student, no job work if you're employed)

## Why
We've been procrastinating too much on my side projects even though we have so much fun doing them - feels like we're not alone in this and want to keep others accountable for working on what they're passionate about  
The vibes will probably be immaculate and I hope the people that come will be friends with each other

## Snacks
Bring Your Own: yes + bring some if you want to (absolutely not necessary)
Tea or coffee will be free - as always. ;)

## Rules/guidelines on how to act
- **Focus**: DON'T work on your main thing (school or your job) - if you don't know what to work on, I'm sure we can help you find something
- **Behavior**: Act like a host, include people in conversations, talk to people even if they're strangers, offer to help out and clean up, don't network if you're here to network I will be so sad, come to build cool stuff; it is about building cool stuff together
- **Extras**: Bring snacks if you're feeling kind

If you have something that you would like to show or talk about, please get in touch with me ASAP. Always feel free to reach out if you want to collaborate or just want to chat about anything! socratica@tuta.io or [type it out directly for us all to see](https://docs.google.com/document/d/1EzeNMlv-rt5qwNSN4ocTs5WnI2wp0AEtBVDjFAcHOAQ/edit?usp=sharing)

## Orgin
The project started a few years ago at an institute in Berlin, aimed at helping colleagues overcome writer's block. That space was designed for individuals working on articles to focus away from distractions, hold each other accountable with "check-ins," and set internal deadlines. 

In terms of asking people where a project is, I have a vague idea that instead of asking "Are you still working on project X", you can instead ask:

**Q:** "Hey, what projects are you working on/prioritizing at the moment?"

**A:** "A is happening today. I want to get onto B at the end of the week. C is happening in the background. I haven't forgotten D, don't worry."

**Q:** (Knows that X is not happening)

_On a more personal note, Socratica is a alsooo homage to my mom who passed away a year ago at a Meetup.com event. Jokes aside, ever since that shit I understood the true meaning of [Bystander Effect](https://onlinelibrary.wiley.com/doi/full/10.1002/ab.21853) it is this phenomenon where strangers are less likely to help in emergency situation when there’s a group around (story of how my mother died). One habit I worked hard to instill in my own head was that if I’m in a crowd that’s asked to do something, I silently count off three seconds. If nobody else responds, I either decide to do it or decide not to do it and I say that. If I do not have a smartphone I say "You in the green shirt, call 112, it is better to be sure. I like this habit, because the Bystander Effect is dumb. Several times now it’s pushed me to step forward in circumstances where I otherwise wouldn’t have, thinking maybe someone else would. Take care of the people around! Also, my mom spoke about starting similar events and it kills me to know she can't be a part of it!! Let's make it fun!_

That's all, folks! And for whatever it's worth, take care of yourself and the people around you.